
### Open Banking D8

##### Installation

###### On your host
- Clone the project

- `cd openbanking-d8`

- Run `docker-compose up -d` 

- Run `docker-compose ps` to achieve the name of Drupal container


- Run `docker exec -ti openbanking_d8_drupal_1 /bin/bash` to enter Drupal container
###### On Drupal container

By running last command, you'll be entered  into the Drupal container. After that, your container prompt will be at the `/app` folder. Then go to the `web` directory beneath the `/app`:

`cd web`.

Then run `composer install`.

After downloading php packages from Packagist and Drupal repositories, you can install your project via `Drupal Console` which already have been installed via composer. Run `./../vendor/bin/drupal site:install`. Then choose profile `Configuration installer` in the wizard.
 Then Provide database name and password mentioned in `docker-compose.yml` file which exists in root of the cloned repository.

##### Post Installation
 After that the installation was finished, you could change username and password for the user with uid `1`. for doing this, first go to the Drupal container and then face your prompt into `web` and finally run `./../vendor/bin/drupal user:login:url`.
  Now you are be able to achieve a url in which you can change username and password for any user and uid you want. You can also do int by `./../vendor/bin/drupal user:password:reset`
 
- Openbanking library
 
Clone openbanking library in to the `web/libraries` directory. Then go to the `app` folder and run `composer update`, so that the library and it's dependencies will be installed.
##### Development

Whenever you want to change an entity or any action in which the configuration can be affected, please export that configuration files so that they can be tracked through source control.
So after changing in the configuration, enter the Drupal container and head your prompt into `web`, then run `./../vendor/bin/drupal config:export` or `./../vendor/bin/drupal ce` for exporting the changed config files.

Finally other people who want to apply your change into their project, must run `./../vendor/bin/drupal config:import` or `./../vendor/bin/drupal ci` to import the configuration files.

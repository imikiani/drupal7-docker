FROM drupal:7-apache
RUN useradd -ms /bin/bash iman
RUN apt-get update && apt-get install -y \
	curl \
	git \
	mysql-client \
	vim \
	wget \
	zlib1g-dev \
	libicu-dev \
	libxml2-dev \
	g++ \
	zip \
    unzip

RUN docker-php-ext-install intl
RUN docker-php-ext-install soap
# Redis
RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis


# DEPENDENCIES FOR KAFKA
RUN apt-get update --fix-missing \
    && apt-get install -y  python

# KAFKA
RUN git clone https://github.com/edenhill/librdkafka.git \
    && ( \
        cd librdkafka \
        && ./configure \
        && make \
        && make install \
    ) \
    && pecl install rdkafka \
    && echo "extension=rdkafka.so" > /usr/local/etc/php/conf.d/rdkafka.ini


# Xdebug
RUN pecl install xdebug-2.7.2 && docker-php-ext-enable xdebug
RUN echo 'zend_extension="/usr/local/lib/php/extensions/no-debug-non-zts-20170718/xdebug.so"' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_port=9001' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_connect_back=1' >> /usr/local/etc/php/php.ini

RUN echo 'xdebug.remote_mode=req' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_log=/tmp/xdebug_remote.log' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_host=127.0.0.1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_autostart=1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_handler=dbgp' >> /usr/local/etc/php/php.ini

# Composer and drush
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php && \
	mv composer.phar /usr/local/bin/composer && \
	php -r "unlink('composer-setup.php');" && \
	composer g require drush/drush:8.*


RUN rm -rf /var/www/html/*

COPY apache-openbanking.conf /etc/apache2/sites-enabled/000-default.conf


COPY envvars /etc/apache2/envvars

WORKDIR /app
